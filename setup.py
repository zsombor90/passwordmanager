from setuptools import setup, find_packages

long_desc = open("README.md").read()
version = open("version.txt").read()
requirements = open("requirements.txt").read().split("\n")[1:]

setup(
    name="pypwd",
    version=version,
    license="@Continental Internal",
    author="Zsombor Egyed @ ADAS DLDI",
    author_email="zsombor.egyed@continental-corporation.com",
    description="Password Manager package",
    long_description=long_desc,
    long_description_content_type="text/markdown",
    url="git+http://github.conti.de/ADAS/PasswordManager.git",
    packages=find_packages(),
    install_requires=requirements,
    data_files=[('', ['version.txt']),
                ('', ['requirements.txt'])],
    classifiers=(
        "Programming Language :: Python :: 2, 3",
        "Operating System :: Windows",
        "Intended Audience :: Developers",
    )
)
