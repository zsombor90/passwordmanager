from pypwd import PasswordManager

pm = PasswordManager()

user_list = ['username1', 'username2']

# set passwords for more user (userlist)
pm.set_users_password(user_list)

# get plain text password for a user
passwd_for_user1 = pm.get_user_password('username1')

# delete one user
pm.delete_user('username2')

# set/update one user password
pm.set_user_password('username1')

# get all user credentials (will return a dict)
user_creds = pm.get_all_user_credentials()
for user_name in user_creds:
	print(user_name, user_creds[user_name])

# delete all user credentials (it will delete all of your users and your secret key)
pm.delete_all_user_credentials()

# if you would like to use the same user(s) w/ different password(s) (e.g. in different environment (dev, prod etc.))
# you can set your own prefix (and key name as well)

pm = PasswordManager(prefix='mongodb_dev_', key_name='my_key')
