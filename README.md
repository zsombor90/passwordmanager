# Password Manager

## Description
It is a python package for managing user-password pairs. 
It will store your credentials in environment variables in encrypted way. 
It was tested on Windows with Python 3 and Python 2 as well. 

## Install the package

If your windows env contains http_proxy, you should unset it first:

```
# cmd
set http_proxy=

# git bash
http_proxy=
```

Set your proxy to the cias (pip.ini or w --proxy, but maybe it will work without proxy as well).
```
# install latest version
pip --proxy http://cias3basic.conti.de:8080 install git+http://github.conti.de/ADAS/PasswordManager.git


# to get a specific version
pip --proxy http://cias3basic.conti.de:8080 install git+http://github.conti.de/ADAS/PasswordManager.git@v0.1.2


# if you would like to put it into your requirements.txt
git+http://github.conti.de/ADAS/PasswordManager.git#egg=pypwd

# or w/ version
git+http://github.conti.de/ADAS/PasswordManager.git@v0.1.2#egg=pypwd

```

## Examples
```
from pypwd import PasswordManager

pm = PasswordManager()

user_list = ['username1', 'username2']

# set passwords for more user (userlist)
pm.set_users_password(user_list)

# get plain text password for a user
passwd_for_user1 = pm.get_user_password('username1')

# delete one user 
pm.delete_user('username2')

# set/update one user password
pm.set_user_password('username1')

# get all user credentials (will return a dict)
user_creds = pm.get_all_user_credentials()
for user_name in user_creds:
	print(user_name, user_creds[user_name])

# delete all user credentials (it will delete all of your users and your secret key)
pm.delete_all_user_credentials()

# if you would like to use the same user(s) w/ different password(s) (e.g. in different environment (dev, prod etc.))
# you can set your own prefix (and key name as well)

pm = PasswordManager(prefix='mongodb_dev_', key_name='my_key')

```

## Warning 
You have to restart your session (IDE/Command line) after set and delete commands.
Please be aware that setup (set_user(s)_password) and deletion takes some time (several seconds).
