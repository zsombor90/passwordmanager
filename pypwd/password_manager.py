import os
from cryptography.fernet import Fernet
from easygui import passwordbox, ynbox, buttonbox

power_shell_set_env = "powershell.exe [Environment]::SetEnvironmentVariable('{}',{},'User')"


class PasswordManager:
    def __init__(self, prefix='pwm_', key_name='key'):
        """Default constructor.

        :param prefix: Your environment variables will be prefixed w/ this value
        :param key_name: The name of your encrypt/decrypt key env var, will be (prefix+key_name)
        """
        self.prefix = prefix
        self.key_name = key_name

    @staticmethod
    def set_env_var(os_env_name, os_env_val):
        """It will set/update an environment variable.

        :param os_env_name: The name of your environment variable
        :param os_env_val: The value of your environment variable

        .. warnings:: IIt will work only after you restarted your IDE/commandline.
        """
        if os_env_name in os.environ:
            warning_msg = ('Your {} variable already exists! Are you sure you want to override it?'.format(os_env_name))
            if ynbox(msg=warning_msg, title="Warning!") is False:
                return  # do not override

        os.environ[os_env_name] = os_env_val
        os.system("SETX {0} {1}".format(os_env_name, os_env_val))

    @staticmethod
    def unset_env_var(os_env_name):
        """It will unset/delete an environment variable.

        :param os_env_name: The name of your environment variable

        .. warnings:: It will work only after you restarted your IDE/commandline.
        """
        os.system(power_shell_set_env.format(os_env_name, '$null'))

    def get_configured_env_vars(self):
        return [env_var.lower() for env_var in os.environ if env_var.lower().startswith(self.prefix.lower())]

    def encrypt_password(self, plain_text_password):
        """It will encrypt your password (text).

        :param plain_text_password: your password in plain text
        :return: encrypted password
        :rtype: str
        """
        key = self.get_crypto_key()
        return Fernet(key).encrypt(plain_text_password.encode('utf-8')).decode('utf-8')

    def decrypt_password(self, encrypted_password):
        """It will decrypt your encrypted password (text).

        :param encrypted_password: encrypted password (str)
        :return: decrypted password (plain text)
        :rtype: str
        """
        key = self.get_crypto_key()
        return Fernet(key).decrypt(encrypted_password.encode('utf-8')).decode('utf-8')

    def get_crypto_key(self):
        """Try to read key from env var, if doesn't exists it will generate a key and save it to env var

        :return: a key for encrypt/decrypt
        :rtype: bytes
        """
        key_env_var_name = self.prefix + self.key_name
        if key_env_var_name in os.environ:
            return os.environ[key_env_var_name].encode('utf-8')

        key = Fernet.generate_key()
        self.set_env_var(key_env_var_name, key.decode('utf-8'))
        return key

    def set_users_password(self, user_list):
        """It will iterate over user_list, ask password(s) and save the encrypted passwords to env var

        :param user_list: a list w/ your users
        """
        for user_name in user_list:
            self.set_user_password(user_name)

    def set_user_password(self, user_name):
        """Ask user password and save the encrypted passwords to env var

        :param user_name: name of your user
        """
        plain_text_pass = passwordbox('set password for {}:'.format(user_name.replace(self.prefix, '')))
        if plain_text_pass:
            encrypted_pass = self.encrypt_password(plain_text_pass)
            self.set_env_var(self.prefix + user_name, encrypted_pass)

    def delete_user(self, user_name):
        self.unset_env_var(self.prefix + user_name)

    def delete_all_user_credentials(self):
        configured_env_vars = self.get_configured_env_vars()
        for env_var in configured_env_vars:
            self.unset_env_var(env_var)

    def get_all_user_credentials(self):
        configured_env_vars = self.get_configured_env_vars()
        user_creds = {}
        for env_var in configured_env_vars:
            if not env_var.endswith(self.key_name.lower()):
                user_creds[env_var.replace(self.prefix.lower(), '')] = self.get_user_password(
                    env_var.replace(self.prefix.lower(), ''))

        return user_creds

    def get_user_password(self, user_name):
        """

        :param user_name: name of your user
        :return: plain text password for your user
        :rtype: str
        """
        try:
            return self.decrypt_password(os.environ[self.prefix + user_name])
        except KeyError:
            msg = "Your user '{}' not set yet. Maybe you just forgot to restart your session (IDE/Command line). " \
                  "Would you like to set your user now?".format(user_name)
            choices = ["Yes", "No", "Exit"]
            reply = buttonbox(msg, choices=choices)

            if reply == choices[0]:
                self.set_user_password(user_name)
                return self.decrypt_password(os.environ[self.prefix + user_name])
            if reply == choices[2]:
                exit()
